<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// Cria uma nova instância do PHPMailer
$mail = new PHPMailer;

// Definição do idioma
$mail->setLanguage('pt_br', 'PHPMailer/language/');

// Informa ao PHPMailer para usar SMTP
$mail->isSMTP();

/**
 * Habilita debug SMTP
 * 
 * 0 = off (para o uso de produção)
 * 1 = Menssagens do cliente
 * 2 = Menssagens do cliente e servidor
 */

 $mail->SMTPDebug = 2;

 /**
  * Define o nome do servidor de e-mail
  * use $mail->Host = gethostbyname('smtp.gmail.com');
  * se sua rede não suporta SMTP over IPv6
  */

  $mail->Host = 'smtp.gmail.com';

  /**
   * Define o número da porta SMTP - 587 para autenticação TLS,
   * a.k.a. RFC4409 SMTP submission
   */
  $mail->Port = 587;

  // Define o sistema de criptografia a usar - ssl (depreciado)
  // ou tls 
  $mail->SMTPSecure = 'tls';

  // Se vai usar SMTP authentication
  $mail->SMTPAuth = true;

  // Usuário para usar SMTP authentication
  // Use o endereço completo do e-mail do GMail
  $mail->Ursename = "developerphp831@gmail.com";

  // Senha para SMTP authentication
  $mail->Password = "4linux@DevPHP";

  //Define o rewmetente 
  $mail->setFrom('developerphp831@gmail.com', 'Curso');

  //Define o endereço para resposta
  $mail->addReplyTo('developerphp831@gmail.com', 'Curso');

  // Define o destinatário
  //$mail->addAddress('<e-mail do destinatário>', 'Nome Dele')
  $mail->addAddress('luisabsenra@outlook.com', 'Luísa de Barros');


  //Define o assunto 
  $mail->Subject = 'Teste de PHPMailer com GMail SMTP';

  // Define o formato da menssagem para HTML
  $mail->isHTML(true);

  //Corpo da menssagem 
  $mail->Body = 'Uma menssagem em HTML <b>em negrito</b>';

  // Corpo alternativo caso o cliente de e-mail não suporte HTML
  $mail->AltBody = 'Menssagem em texto simples';

  // Envia a menssagem e verifica os erros
  if (!$mail->send()) {
      echo "Erro no Mailer: " . $mail->ErrorInfo;
  } else {
      echo 'Menssagem eniviada<br>';
  


  // Salvando a menssagem na caixa de e-mails enviados.

    if (save_mail($mail)) {
          echo 'Mensagem salva!<br>';
       }
    }
    
    /**
     * Comandos IMAP requerem a extensãpo PHP IMAP
     * Documentação: https://php.net/manual/en/imap.setup.php
     * Funções IMAP: https://php.net/manual/en/book.imap.php
     * 
     * Você pode usar imap_getmailboxes($imapStream, '/imap/ssl')
     * para obter uma lista de pastas e etiquetas disponíveis
     */

     function save_mail($mail)
     {
        // Pasta para onde env8iar a cópia
        $path = '{imap.gmail.com:993/imap/sll}INBOX';

        // Abre uma conexãoi IMAP usando o mesmo usuário e senha
        // usados para o SMTP
        $imapStream = imap_open($path, $mail->Username,
                                $mail->Password);
        
        $result = imap_append(imapStream, $path,
                              $mail->getSentMIMEMessage());

        imap_close($imapStream);

        return $result;
     }
