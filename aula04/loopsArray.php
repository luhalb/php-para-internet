<?php
$cores = [
    'Ciano',
    'Magenta',
    'Yellow',
    'Black'
];

while (list(,$cor) = each($cores)) {
    echo "<center>$cor<br></center>";
}

echo'<hr>';

for ($chave = 0; $chave < count ($cores); $chave++) { 
    echo "<center>$chave - {$cores[$chave]}<br>";
}

echo'<hr>';

$pessoas = [
    [
        'nome'           => 'Paulo Antunes',
        'dataNascimento' => '1985-05-13',
        'email'          => 'paulo.antunes@gmail.com'
    ],
    [
        'nome'           => 'Joana Nascimento',
        'dataNascimento' => '1978-07-15',
        'email'          => 'joana@hotmail.com'
    ],
];

foreach ($pessoas as $chave => $pessoa) {
    echo'<hr>';
    echo "<center>Pessoa => $chave<br>";

    foreach ($pessoa as $campo => $dado) {
        echo "<center>$campo: $dado<br>";
    }

    echo '<hr>';
}
?>  