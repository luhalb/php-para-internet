<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EXERCICIO PHP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body bgcolor='#f5cfc9'>
<?php
$cores = [
    'Ciano',
    'Magenta',
    'Yellow',
    'Black'
];

foreach ($cores as $cor) {
    echo "<center><font color='#0097c1'><h3>$cor<br></font></h3>";
}

echo "<hr>";

foreach ($cores as $chave => $cor) {
    echo "<center><font color='#0097c1'><h3>$chave - $cor<br></font></h3>";
}

?>  
</body>
</html>

