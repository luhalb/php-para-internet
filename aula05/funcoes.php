<?php
/**
 *  Sintaxe básica para declarações de funções
 */

 function nomeDaFuncao($arg_1, $arg_2, /* ..., */ $arg_n)
 {
    echo "Exemplo de função\n";

    return;
 }

 /* ------------------------------------------------------------------------------------- */

 function negrito($texto)
 {
     return "<strong>$texto</strong>";
 }

 echo negrito('Olá mundo!');