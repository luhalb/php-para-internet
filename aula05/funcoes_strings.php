<?php

$artilheiro = 'Philippe Coutinho';
$gols       = '19';

$formato = 'O artilheiro da copa, %s, tem %d gols.';

printf($formato, $artilheiro, $gols);

echo '<hr>';

$frase = sprintf($formato, $artilheiro, $gols);

echo $frase;

echo '<hr>';

$num = 5;
$local = 'árvore';

$formato = 'A %2$s contém %1$d macacos';
//echo sprintf($formato, $num, $local);
echo vsprintf($formato, [5, 'árvore']);

echo '<hr>';

// Fornece: <body texet='black'>

$bodytag = str_replace('%body%', 'black', '<body text=%body%>');

echo '<textarea>' . $bodytag . '</textarea>';

echo '<hr>';

//Fornece Hll Wrld f PHP

$vogais = [
    'a',
    'e',
    'i',
    'o',
    'u',
    'A',
    'E',
    'I',
    'O',
    'U'
];
$apenasConsoantes = str_replace($vogais, '', 'Hello World of PHP');

echo $apenasConsoantes;

echo '<hr>';

$frase = "<h3><center><font color='#50ff43'>Você comeria frutas, vegetais, e fibras todos os dias</font></h3>";

$saudavel = [
    'frutas',
    'vegetais',
    'fibra'
];

$saboroso = [
    'pizza',
    'refrigerante',
    'sorvete'
];

$novaFrase = str_replace($saudavel, $saboroso, $frase);
echo $novaFrase;

echo '<hr>';

$str = "<font color='5ab8ff'>A 'quote' is <b>bold</b></font>";
// Saída: A 'quote' is &lt(menor que);b&gt(maior que);

echo htmlentities($str);

echo '<hr>';

//Saída: A &#039;

echo htmlentities($str, ENT_QUOTES);
echo '<hr>';
echo $str, '<br>';

echo htmlspecialchars_decode($str), '<br>';


// note que aqui as aspas não são convertidas
echo htmlspecialchars_decode($str, ENT_NOQUOTES), '<br>';
echo '<hr>';

// aspas convertidas
echo htmlspecialchars_decode($str, ENT_QUOTES), '<br>';

echo '<hr>';

echo '<pre>';

$texto = "\t\tAqui estão algumas palavras :) ...   ";
$binario = "\x09String de exemplo\x0A";
$hello = "Hello World";

var_dump($texto, $binario, $hello);

echo '<br>';

$trimed = trim($texto);
var_dump($trimed);

echo '<br>';

$trimed = trim($texto, " \t.");
var_dump($trimed);

echo '<br>';

$trimed = trim($hello, "Hdle");
var_dump($trimed);

echo '<br>';

// Remove os caracteres de controle ASCII no inicio e no fim  de $binario
// (DE 0 a 31 inclusive)

$clean = trim($binario, "\x00..\x1F");
var_dump($clean);

echo '<hr>';

$string = 'hello world!';
$string = ucfirst($string); //Hello world;

echo $string;

echo '<hr>';

$string = 'hello world!';
$string = ucwords($string); //Hello World;

echo $string;

echo '<hr>';

$string = 'hello world!';
echo strtoupper($string), '<br>'; //HELLO WORLD!
echo strtolower($string), '<br>'; //hello world!


echo '<hr>';

$string = "Texto com quebra de linha\nA partir daqui na linha de baixo!";
echo $string, '<br>';
echo nl2br($string);

echo '<hr>';



for ($i=0; $i < 100 ; $i++) { 
    echo '<br>';
}