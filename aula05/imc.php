<?php

function imc($peso, $altura)
{
    return $peso / ($altura*$altura);
}

echo imc(120, 1.85);

echo '<hr>';

function iourguteira($sabor, $tipo = 'azeda')
{
    return "Fazendo uma taça de $sabor $tipo";
}

echo iourguteira('framboesa'), '<br>';
echo iourguteira('morango','com chantilly'), '<br>';

// argumentos com valor padrão devem smpre estar
// a direita (como não fazer)
echo '<hr>';

function iourguteira2($tipo = 'azeda', $sabor)
{
    return "Fazendo uma taça de $sabor $tipo";
}

echo iourguteira2('framboesa'), '<br>';
echo iourguteira2('morango','com chantilly'), '<br>';


