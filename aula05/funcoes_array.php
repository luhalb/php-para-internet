<?php

$a =[1, 2, 3, 4, 5];

$b = array_map(function ($elemento) {
    return $elemento ** 3;
}, $a);

echo '<pre>';
print_r($a);
echo '<pre>';
print_r($b);

echo '<hr>';

$array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

echo '<pre>';

echo "Impares: \n";
print_r(array_filter($array, function($elemento) {
    return ($elemento % 2);
}));

echo '<br>';

echo "Pares: \n";
print_r(array_filter($array, function($elemento) {
    return !($elemento % 2);
}));

echo '<hr>';

$array1 = ['a' => 'verde', 'vermelho', 'azul', 'vermelho'];
$array2 = ['b' => 'verde', 'amarelo', 'vermelho'];

$result = array_diff($array1, $array2);

$resulta = array_diff($array2, $array1);

echo '<pre>';
print_r($result);
echo '<br>';
print_r($resulta);


echo '<hr>';

$arraya = ['azul', 'vermelho', 'verde'];

$chave = array_search('verde', $arraya);

echo $chave, "<hr>";

$chave = array_search('vermelho', $arraya);

echo $chave, "<hr>";

$a = [2, 4, 6, 8];

var_dump($a);

echo '<pre>';

echo "soma(a) = " . array_sum($a), '<br>';

$b = ['a' => 1.2, 'b' => 2.3, 'c' => 3.4];

var_dump($b);
echo "soma(b) = " . array_sum($b), '<br>';



$entrada = ['a' => 'verde', 
                   'vermelho',
            'b' => 'verde',
                   'azul'
];



$result = array_unique($entrada);





echo '<hr>';

echo '<pre>';
print_r($result);

echo '<hr>';


$os = ['Mac', 'NT', 'Irix', 'Linux'];

if (in_array('Irix', $os)) {
    echo 'Tem Irix';
}

echo '<br>';
if (in_array('Mac', $os)) {
    echo 'Tem Mac';
}

echo '<hr>';

echo count($os);

echo '<hr>';

$serial =serialize($os);

echo $serial,'<br>';

$array = unserialize($serial);

var_dump($array);

echo '<hr>';
 









for ($i=0; $i < 100 ; $i++) { 
    echo '<br>';
}
