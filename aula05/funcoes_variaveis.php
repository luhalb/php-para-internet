<?php

$variavel = 0;


//vazio = '', 0, [], false 

if(empty($variavel)) {
    echo 'Esta variável é considerada vazia!', '<br>';
}

echo '<hr>';
$variavel = '';

// isset só retorna false se a váriavel não foi declarada,
// contém null ou foi passada como parâmetro para unset()
if(isset($variavel)) { 
    echo 'Variável foi inicializada!', '<br>';
} else {
    echo 'Variável não foi inicializada!', '<br>';
}

echo '<hr>';

$sim = ['isto', 'é', 'um array'];

echo is_array($sim) ? 'É um array' : 'Não é um array';

echo '<hr>';

$nao = 'Isto é uma string';

echo is_array($nao) ? 'É um array' : 'Não é um array';

echo '<hr>';

$testes = [
    '42',
    1337,
    0x539, //o 0x mostra que e decimal
    02471, // numéro octal
    0b10100111001

];


print_r($testes);

foreach ($testes as $elemento) {

    # code...
}