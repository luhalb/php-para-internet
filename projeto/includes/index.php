<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>HOME | Desenvolvimento Web com PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="../lib/bootstrap-4.3.1-dist/css/bootstrap.min.css">
        
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div vlass="col-md-12">
                <!-- Topo // -->
                <?php
                    include 'includes/topo.php';
                ?> 
            </div>
            <div class="row" style="min-height: 500px">
                <div class="col-md-2">
                <!-- Menu // -->
                <?php
                    include 'includes/menu.php';
                ?>
            </div>
            <div class="col-md-10" style="padding-top: 50px">
            <!-- Contéudo // -->
            <h2>Home</h2>
            <?php   
                include 'includes/busca.php';
            ?>

            <?php
                require_once 'includes/funcoes.php';    
                require_once 'core/conexao_mysql.php';    
                require_once 'core/sql.php';    
                require_once 'core/mysql.php';    

                foreach ($_GET as $indice => $dado) {
                    $$indice = limparDados($dado);
                }

                $data_atual = date('Y-m-d H:i:s');

                $criterio = [
                    ['data_postagem', '<=', $data_atual]
                ];

                if (!empty($busca)) {
                    $criterio[] = [
                        'AND',
                        'texto',
                        'like',
                        "%{busca}%"
                    ];
                }

                $posts = buscar(
                    'post',
                    [
                        'titulo',
                        'data_postagem',
                        'id',
                        '(select nome
                            from usuario
                            where usuario.id = post.usuario_id) as nome'
                    ],
                    $criterio,
                    'data_postagem DESC'
                );
            ?>
            <div class="list-group">
                <?php
                    foreach ($posts as $post) :
                        $data = data_create($post['data_postagem']);
                        $data = data_format($data, 'd/m/Y H:i:s');
                ?>
            </div>   
        </div>                    
    </body>
</html>;