<?php

function insert(string $entidade, array $dados) : string 
{
    $instrucao = "INSERT INTO {$entidade}";
    $campos    = implode(', ', array_keys($dados)); // listar os campos
    $valores   = implode(', ', array_values($dados)); //listar os dados

    $instrucao .= " ({$campos})";
    $instrucao .= " VALUES ({$valores})";

    return $instrucao;
}


function update (strind $entidade, array $dados, array $criterio = []) : string // criterio é opcional
{
    $instrucao = "UPDATE {$entidade}";

    foreach ($dados as $campo => $dado) {
        $set[] = "{$campo} = {$dado}";
    }

    $intrucao .= ' SET ' . implode(', ', $set);

    if (!empty($criterio)){
        $intrucao .= ' WHERE ';

        foreach ($criterio as $expressao) {
            $intrucao .= ' ' . implode(' ', $expressao);
        }
    }

    return $instrucao;
}

function delete(string $entidade, array $criterio = []) : string{
    $instrucao = "DELETE FROM {$entidade}";
    
    if (!empty($criterio)) {
        $instrucao .= ' WHERE ';

        foreach ($criterio as $expressao) {
            $instrucao .= ' ' . implode (' ', $expressao);
        }
    }
    return $instrucao;
}

    function select(string $entidade, array $campos, array $criterio = [], string $ordem = null) : string {
        $instrucao = "SELECT " . implode(', ', $campos);
        $instrucao .= " FROM {$entidade}";
    
        if (!empty($criterio)) {
            $instrucao .= ' WHERE ';

            foreach ($criterio as $expressao) {
                $instrucao .= ' ' . implode(' ', $expressao);
                
            }
        }

        if(!empty($ordem)) {
            $instrucao .= " ORDEM BY $ordem";
        }

        return $instrucao;
    }







?>