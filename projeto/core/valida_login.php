<?php

$_SESSION['url_retorno'] = $_SERVER['PHP_SELF'];

if (!isset($_SESSION['login'])) {
    header('Location: /500/projeto/login_formulario.php');
    exit;
}