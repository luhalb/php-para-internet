<?php

$servidor = '127.0.0.1';
$usuario  = 'developer';
$senha    = '4linux';
$banco    = 'blog';

$conexao = pg_connect("host=$servidor
                       dbname=$banco
                       user=$usuario
                       password=$senha");

if (!$conexao) {
    echo 'Erro: Não foi possível conectar ao PostgreSQL.<br>' . PHP_EOL;
    exit;
}

echo 'Sucesso: Uma conexão ao PostgreSQL foi estabelecida!<br>' . PHP_EOL;
echo 'Informação do Host: ' . pg_host($conexao) . '<br>' . PHP_EOL;

pg_close($conexao);