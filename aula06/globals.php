<?php

function teste()
{
    $var = 'variável local';
    echo '$var no escopo global: ' . $GLOBALS['var'] . '<br>';
    echo '$var no escopo local: ' . $var . '<br>';
}

$var = 'Conteúdo de exemplo';

echo '<pre>';

teste();

var_dump($GLOBALS);
