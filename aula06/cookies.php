<?php

$uf = 'SP';

/**
 * Parâmetros para a função setcookies
 * setcookie(name, value, expire, path, domain,
 * security, httponly)
 */

setcookie('uf', $uf);//tempo de vida da sessão
setcookie('uf', $uf, time() + 3600); // Expira em uma hora
setcookie('uf', $uf, time() + 3600, '/cookies/', '.exemplo.com', 1, 1); 

if($_COOKIE['uf']) {
    echo $_COOKIE['uf'];

    echo '<pre>';
    print_r($_COOKIE);
}

// Excluindo um cookie - Fazendo ele expirar -
// Configura a expiração para uma hora atrás

// setcookie('uf', '', time() - 3600);

// Envia os cookies

setcookie('visitantes[nome]', 'Janis Joplin');
setcookie('visitantes[email]', 'janis@peacelove.com');
setcookie('visitantes[data]', date('Y-m-d H:i:s'));


echo '<pre>';

print_r($_COOKIE);