<?php

// session_start();


/**
 * Se não houver necesssidade de alterar nada na seção,
 * pode-se apenas lê-la e já fechá-la para evitar
 * que o arquivo de sessão seja travado e então bloqueie
 * outras páginas
 */
session_start([
    'cookie_lifetime' => 86400,  // Envia o cookie persistente q dura um dia
    'read_and_close'  => true //Lê a sessão e a encerra
]);

$status = [
    0 => 'Sessões desabilitadas',
    1 => 'Sessões habilitadas [Nenhuma criada]',
    2 => 'Sessões habilitadas [Uma criada]'
];



echo 'Status da sessão: "' . $status [session_status()] . '"';
