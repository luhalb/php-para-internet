<?php

$nome = 'Carlos Souza';

$setor = [
    'dev' => 'PHP',
    'ops' => 'Docker'
];

// Não Concatenados

echo "$nome do setor de {$setor['dev']}<br>";
echo "{$nome} do setor de {$setor['ops']}<br>";

echo '<hr>';
//Concatenados

echo $nome . ' do setor de ' . $setor['dev'] . '<br>';
echo $nome . ' do setor de ' . $setor['ops'] . '<br>';