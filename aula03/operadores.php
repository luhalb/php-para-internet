<?php
echo "<pre>";

$a = '5';

var_dump($a); //$a é string;

var_dump(+$a); //$a é convertido para int positivo

var_dump(-$a); //$a é convertido para int negativo


$b = '5.3';

var_dump($b); //$b é string;

var_dump(+$b); //$b é convertido para int positivo

var_dump(-$b); //$b é convertido para int negativo


$c = $a + $b;
var_dump($c); // resultado é 10

$c = $a - $b;
var_dump($c); // resultado é -0.3

$c = $a * $b;
var_dump($c); // resultado é 26.5

$c = $a / $b;
var_dump($c); // resultado é 0.94339622641509

$c = $a % $b;
var_dump($c); // resultado é 0

$c = $a ** $b;
var_dump($c); // resultado é 5064.5518646649








