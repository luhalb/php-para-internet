<?php
echo '<pre>';
$var = 10;

$cast = (boolean) $var; // torna-se booleano (>=1 verdadeiro & <=0 falso)
var_dump($cast);

$cast = (int) $var; // torna-se inteiro
var_dump($cast);

$cast = (float) $var; // torna-se float
var_dump($cast);

$cast = (string) $var; // torna-se string
var_dump($cast);

$cast = (object) $var; // torna-se objeto
var_dump($cast);

$cast= (unset) $var; // torna-se null
var_dump($cast);