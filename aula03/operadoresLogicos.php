<?php

echo '<pre>';

$a = true;
$b = false;
$c = true;

var_dump(($a and $b)); // falso

var_dump(($a or $b)); // true

var_dump(($a or $c)); // true

var_dump(($a xor $b)); // true

var_dump(($a xor $c)); // falso

var_dump((!$a)); // falso

var_dump(($a && $b)); // falso

var_dump(($a || $b)); // true

