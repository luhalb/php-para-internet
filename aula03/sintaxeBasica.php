<?php
    // Estilo de comentário de uma linha
    echo 'Isto é um teste'; // como mostrar algo na tela

    /*
    Este é um comentário de múltiplas linhas
    ainda outra linha de comentário 
    */
?>

<?php echo 'outro teste <br>'; ?>    

<?php 

# Este é um comentário no Estilo Shell
  echo 'Nós omitimos a ultima tag de fechamento';    
?> 