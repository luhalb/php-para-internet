<?php

    // Uso muito comum antes da introdução do NULL coalescing (PHP7)
    $nome = isset($_GET['nome']) ? $_GET['nome'] : 'Sem nome';
    
    echo'<hr>';

    // função empty() é mais adequada
    //  pois só retorna true se a variiável contiver null,'', 0
    $nome = !empty($_GET['nome']) ? $_GET['nome'] : 'Sem nome';
    echo $nome;

    echo'<hr>';

    //NULL coalescing em ação
    $nome = $_GET['nome'] ?? 'Sem nome';

