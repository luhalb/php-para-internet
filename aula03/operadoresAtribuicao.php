<?php

echo '<pre>';

$a = 3;

$a += 2;
var_dump($a); // resultado é 5


$a -= 2;
var_dump($a); // resultado é 3


$a *= 2;
var_dump($a); // resultado é 6


$a /= 2;
var_dump($a); // resultado é 3


$a %= 2;
var_dump($a); // resultado é 1


$a **= 2;
var_dump($a); // resultado é 1


$a .= 2;
var_dump($a); // resultado é 12


